package require sqlite3
if {[catch { 
	cd data
	puts "Creation de zoo.sqlite dans [pwd]"
	sqlite3 db zoo.sqlite
	db eval {CREATE TABLE IF NOT EXISTS animaux(id INTEGER PRIMARY KEY, nom TEXT, espece TEXT)}
	
	
	set animaux [list \
	[list Bob Tigre ] \
	[list Ann Girafe] \
	[list Helen Crocodile] \
	]
	foreach animal animaux {
		set nom [lindex $animal 0]
		set espece [lindex $animal 1]
		db eval {INSERT INTO animaux(nom,espece)VALUES($nom, $espece)}
	}
	db eval {SELECT COUNT(*) c FROM animaux} {
		puts "Il y a $c animaux"
	}
	db close
} err]} {
	puts "Erreur : $err ! $errorInfo ($errorCode)"
}
