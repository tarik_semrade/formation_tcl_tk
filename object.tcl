# Classes et objets
oo::class create Animal {

	variable nom
	variable age
	variable espece
	constructor {n} { set nom $n }
	constructor {{n ""} {a  0} {e ""}} {
		set nom $n
		set age $a
		set espece $e
	}
	destructor { puts "$nom detruit"}
	method setNom {n} { set nom $n }
	method setAge {a} { set age $a }
	method setAgeEnMois {m} { my setAge [expr $m/12] }
	method setEspace {e} { set espece $e }
	method afficher {} { puts "$nom ($espece) a $age ans" }
}
oo::class create Encadreur {
	method encadrer {} {
		puts "*******************"
		my afficher
		puts "*******************"
}
}
oo::class create Reptile {
	superclass Animal
	mixin Encadreur
	variable amphibien
	constructor {{n ""} {a 0} {e ""} {am ""}} {
	next $n $a $e
	set amphibien  $am
	}
	method setAmphibien {am} { set amphibien $am }
	method afficher {} {
		next 
		if {$amphibien} {
			puts "-C'est un amphibien"
		} else {
			puts "- C'est un terrien"
		}
	}
}

set ann [Animal new "Ann"]
$ann setNom "Ann"
$ann setAge 23
$ann setEspace Girafe
$ann afficher

set tom [Animal new "TOM" 8 Tigre]
$tom afficher
$tom destroy

set franck [Reptile new "Phil" 3 Caiman]
$franck afficher