#namespaces
namespace eval Aquarium {

	variable  longueur 27
	variable  largeur 18
	variable  profondeur 6
	namespace export affiche
	namespace ensemble create
}

proc Aquarium::get_volume {} {
	#global longueur largeur profondeur
	variable  longueur
	variable  largeur
	variable  profondeur
	
	return [expr $longueur*$largeur*$profondeur]
}

proc Aquarium::affiche {} {
	puts "Notre aquarium : [get_volume] m3 !3"
}

::puts "Informations :"
Aquarium::affiche

namespace import Aquarium::*
::affiche