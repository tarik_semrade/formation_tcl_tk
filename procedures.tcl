proc ditbonjour {} { puts "Hello Zoo !" }

proc ditbonjouralanimal {{nom ""} {excl ! }} {
	puts "Hello $nom $excl"
}


proc ditbonjouranimaux args {
	foreach animal $args {
		ditbonjouralanimal $animal
	}
}
proc getcri {animal} {
	switch $animal {
	trigre { return rugit }
	girafe { return meugle }
	faucon { return huit }
	default { return cri }
	}
}

#proc incr {nomvariable} { set $nomvariable [expr $nomvariable+1] }
#set i 18
#incr i

proc tarifpour {visiteur} {
	# global tarif
	# return [expr $tarif*visiteurs]
	upvar tarif tarifloc
	return [expr $tarifloc*visiteurs]
}

proc decr {nomvariable} {
	upvar $nomvariable nomvariableloc
	set nomvariableloc [expr $nomvariableloc-1]
}

proc afficheinfos {} {
	set tarif 10.3
	decr tarif
	puts "WE special pour 4 : [tarifpour 4]

}
ditbonjour
ditbonjouralanimal
ditbonjouralanimal "Ann la girafe"
puts "La grirafe [getcri girafe]"
ditbonjouranimaux Ann Tom Carol





