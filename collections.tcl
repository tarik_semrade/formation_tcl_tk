# * boucles 
# while {$a > 0} { set a [expr $a-4] }
# for {set i 0} {$i<5} {incr i} {
#      puts $i
# }
#
# * listes
# set bob [list lion 12 "enclos 4"]
# lappend notes fa sol
# linsert $notes 0 si
# lset animaux2 2 Aigle
# lreplace $animaux2 0 1 Chat Mouton Lion

# llength $animaux2
# lindex $animaux2 1
# foreach a $animaux2 { puts $a }

# 1 : préparer une liste de 4 lieux
# 2 : afficher toutes les trajets visitant 3 lieux différents
set lieux [list {Enclos des singes} Menagerie Voliere Aquarium]
for {set i 0} {$i<[llength $lieux]} {incr i} {
	for {set j 0} {$j<[llength $lieux]} {incr j} {
		for {set k 0} {$k<[llength $lieux]} {incr k} {
			if {$i!=$j && $i!=$k && $j!=$k} {
				puts "- [lindex $lieux $i] [lindex  \
					$lieux $j] [lindex $lieux $k]"
			}				
		}
	}
}

# -ascii -integer ou -real (par défaut : -ascii)
# -sorted : si la list est déjà ordonnée
# -all : renvoie une liste de tous les éléments trouvés
# -start x : commence à l'élément x
# -exact -glob -regexp (par défaut : -glob)
puts "L'aquarium est le lieu n°[lsearch -nocase $lieux aquarium]"
# -ascii -integer ou -real (par défaut : -ascii)
# -nocase
# -increasing ou -decreasing
set lieux_ordonnes [lsort -ascii -nocase -increasing $lieux]
puts $lieux_ordonnes

puts "Lieux :"
# une chaine est une liste de mots
# set lieux2 {{Enclos des singes} Menagerie Voliere Aquarium Buvette}
set lieux2 "{Enclos des singes} Menagerie Voliere Aquarium Buvette"
foreach lieu $lieux2 {
	puts "- $lieu"
}

#Enclos des singes
set premier_lieu [lindex $lieux2 0] 

#Enclos
set premier_mot [lindex $premier_lieu 0] 
puts "Dans cette chaine il y a [llength $premier_mot] mot"
puts "Dans Lions,Tigres,Jaguars : [llength {Lions,Tigres,Jaguars}]"

# * chaines (commande string) :
puts "Dans le 1er lieu, [string length $premier_lieu] caracteres"
puts "Le 1er est : [string index $premier_lieu 0]"
puts "Nettoyé : [string toupper [string trim $premier_lieu]]"
puts "Dernier d : [string last d [string totitle $premier_lieu]]"

# Afficher par ordre alphabetique tous les lieux qui contiennent un e
set lieux_ordonnes [lsort -ascii -nocase -increasing $lieu ]

foreach lieu $lieux_ordonnes {
	if {[string first e $lieu]!=-1} { puts "- $lieu "}
}







#tableaux
set bob(espece) lion
set bob(age) 12
set bob(lieu) { Enclos des fauves }
puts "Bob est un $bob(espece)"
