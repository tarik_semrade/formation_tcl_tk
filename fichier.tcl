if {[catch { [expr 3/0] } err]} {
	puts "Error : $err"
}
# erreurs :
if {[catch { [expr 3/0] } err]} {
	puts "Erreur : $err !"
}
if {[catch { error "Plus de cacahuettes" } err]} {
	puts "Erreur : $err !"
}
if {[catch { 
	error "Plus de cacahuettes" "Enclos des singes" 10
} err]} {
	puts "Erreur : $err ! $errorInfo ($errorCode)"
}
if {[catch { 
	if {![file exists data]} {
		file mkdir data
	}
	cd data
	puts "Repertoire en cours : [pwd]"
	foreach fichier [glob -nocomplain *.*] {
		puts "- $fichier"
	}
	
	# log210408.txt
	set nomfichier "log[clock format [clock seconds] -format %y%m%d].txt"
	puts "Remplissage de $nomfichier"
	set f1 [open $nomfichier a]
	# 2021-04-08 15:47:56 ok
	puts $f1 "[clock format [clock seconds] \
		-format {%Y-%m-%d %H:%M:%S}] ok"
	close $f1
	
	set f2 [open $nomfichier r]
	set nblignes 0
	while { [gets $f2 ligne] >= 0 } {
		incr nblignes
	}
	close $f2
	puts "$nblignes lignes de log dans le fichier d'aujourd'hui"
} err]} {
	puts "Erreur : $err ! $errorInfo ($errorCode)"
}
